import React, { useState, useEffect } from 'react';
import api from './services/api';

import Header from './components/Header';

import './App.css';

/*
 * Componente 
 * Propriedade
 * Estado e Imutabilidade
*/

function App() {
  const [projects, setprojects] = useState([]);

  useEffect(() => {
    api.get('/projects').then(response => {
      setprojects(response.data);
    })
  }, []);

  async function handleAddProject() {


    const response = await api.post('/projects', {
      title: `Novo Projeto ${Date.now()}`,
      owner: "Victor Eduardo"
    });

    const project = response.data;

    setprojects([...projects, project]);
  }

  return (
    <>
      <Header title="Projects" />

      <ul>
        {projects.map(project => <li key={project.id}>{project.title}</li>)}
      </ul>
      <button type="button" onClick={handleAddProject}>Adicionar um projeto</button>
    </>
  )
}

export default App;