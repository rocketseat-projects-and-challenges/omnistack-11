const express = require('express');
const cors = require('cors');
const { uuid, isUuid } = require('uuidv4');

const app = express();

app.use(cors());
/* Adiciona a funcionalidade do express de interpretar json */
app.use(express.json());

/* 
 * GET : Buscar informações do back-end
 * POST : Criar uma informação no back-end
 * PUT / PATCH : Alterar uma informação no back-end
 * DELETE : Deletar uma informação no back-end
 * 
 * Tipos de Parâmetros : 
 * 
 * Query Params : Filtros e Paginação 
 * Route Params : Identificar recursos ( Atualizar / Deletar )
 * Request Body : Conteúdo na hora criar ou editar um recurso (JSON)
 *
 * Middleware : Interceptador de Requisições 
 * ele pode interromper totalmente a requisição ou pode alterar dados da requisição
 * 
 */

const projects = [];

// middleware
function logRequests(request, response, next) {
  const { method, url } = request;
  const logLabel = `[${method.toUpperCase()}] ${url}`;
  console.time(logLabel);
  next();
  console.timeEnd(logLabel);
}

function validateProjectId(request, response, next) {
  const { id } = request.params;

  if (!isUuid(is)) {
    return response.status(400).json({ error: 'Invalid project ID.' });
  }

  return next();
}

// Aciona o Middleware
app.use(logRequests);

// Aqui eu falo em que rotas o middeware deve ser executado
app.use('/projects/:id', validateProjectId);

app.get('/projects', (request, response) => {
  const { title } = request.query;

  // Filtro que busca dentro de title a palavra enviada na requisição 
  const results = title
    ? projects.filter(project => project.title.includes(title))
    : projects;

  return response.json(results);
});

app.post('/projects', (request, response) => {
  const { title, owner } = request.body;

  const project = { id: uuid(), title, owner };

  // Push serve para jogar a informação do projeto dentro do array de projetos 
  projects.push(project);

  // Exibe o projeto recem criado
  return response.json(project);
});

app.put('/projects/:id', (request, response) => {
  const { id } = request.params;
  const { title, owner } = request.body;

  // Busca o projeto dentro do array de projetos e grava na constante projectIndex
  // a posição de onde o porjeto está no array
  const projectIndex = projects.find(project => project.id === id);

  // verifica se o projeto foi encontrado dentro do array caso não retorna erro 
  if (projectIndex < 0) {
    return response.status(400).json({ error: 'Project Not Found !' });
  }
  // cria o projeto com os dados atualizados
  const project = {
    id,
    title,
    owner,
  }

  // agora percorre o array procurando pelo id informado e substitui ele pelo 
  // novo objeto que contem as alterações 
  projects[projectIndex] = project;

  // Retorna o porjeto atualizado
  return response.json(project);
});

app.delete('/projects/:id', (request, response) => {
  const { id } = request.params;

  // Busca o projeto dentro do array de projetos e grava na constante projectIndex
  // a posição de onde o porjeto está no array
  const projectIndex = projects.find(project => project.id === id);

  // verifica se o projeto foi encontrado dentro do array caso não retorna erro 
  if (projectIndex < 0) {
    return response.status(400).json({ error: 'Project Not Found !' });
  }

  // Efetua a Remoção do Indice
  projects.splice(projectIndex, 1);

  return response.status(204).json();
});

app.listen(3333, () => {
  console.log('🚀 BackEnd Started');
});