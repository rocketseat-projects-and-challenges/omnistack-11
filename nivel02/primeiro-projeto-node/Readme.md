## Comandos Utilizados desde o início da aplicação :

> Inicia a criação do Arquivo package.json

```
$ yarn init -y
```

> Instala a dependência do Express

```
$ yarn add express
```

> Instala o Typescript como dependência de desenvolvimento

```
$ yarn add typescript -D
```

> Cria o Arquivo tsconfig.json ( armazena as configurações de como o typescript vai ser executado dentro do projeto )

```
$ yarn tsc --init
```

> Montar a configuração de pastas do projeto

```
|-- Projeto
   |-- src
   |-- models
   |-- repositories
   |-- routes
   |-- services
      |-- server.ts ( Arquivo que é compilado para gerar o ./src/server.js )
|-- .editorconfig
|-- .eslintignore
|-- .eslintrc.json
|-- .gitignore
|-- ormconfig.json
|-- package.json
|-- prettier.config.js
|-- Readme.md
|-- tsconfig.json
|-- yarn.lock

> Descritivo da funcionalidade das Pastas :
     |-- models       : Pasta onde ficam armazenados as classes modelos de dados que a aplicação utiliza;
     |-- repositories : É a conexão entre a persistência de Dados e a rota, geralmente existe 1 repositório por model
     |-- routes       : As Rotas tem a função de receber a requisição , chamar outro arquivo e devolver uma resposta;
     |-- services     : São arquivos de fonte onde ficam armazenados arquivos de regras de negócio da aplicação;

```

#### Configurar arquivo tsconfig.json

> Fazer alteração nos seguintes registros do arquivo :

```
"outDir": "./dist", /* Redirect output structure to the directory. */
"rootDir": "./src", /* Specify the root directory of input files. Use to control the output directory structure with --outDir. */
```

> Instala o arquivo de declarações de tipos do express isso acontece por causa do typescript

```
$ yarn add @types/express -D
```

> Faz o papel de converter o codigo typescript para javascript e tb o papel do nodemon

```
$ yarn add ts-node-dev -D
```

> Executa o projeto

```
$ yarn dev:server
```

> Instala a dependencia do eslint em modo desenvolvimento

```
// Instala o eslint como dependencia de desenvolvimento
$ yarn add eslint -D
// Cria o arquivo do eslint para o projeto
$ yarn eslint --init
```

> Habilitas para o código entender as importasções de aruivos typescript

```
$ yarn add -D eslint-import-resolver-typescript
```

>

```
$ yarn add prettier eslint-config-prettier eslint-plugin-prettier -D
```

> Biblioteca para tratamento de Datas e horas

```
yarn add date-fns
```

## Arquitetura

- Rotas = A função da Rota é Receber a requisição, chamar outro arquivo, devolver uma resposta;
- Model = é o modelo de dados que aplicação usa
- Repositório = é a conexão entre a persistencia de dados e a rota, geralmente tem 1 repositório por model
- Service = Armazena a regra de negócio da Aplicação
  > O Serviço tem uma única e exclusiva funcionalidade

## Docker

```
// Como verificar se a porta do docker esta sendo utilizado por outro aplicativo ( powershell )
$ Get-Process -Id (Get-NetTCPConnection -LocalPort 5432).OwninProcess

// ou para ver todas as portas que estão sendo utilizadas pelo Windows
$ netstat -a -b
```

- Criando imagem do docker ( postgres )

```
$ docker run --name gostack_postgres -e POSTGRES_PASSWORD=docker -p 5432:5432 -d postgres

// Saber se o container foi criado
$ docker ps

// Subir o container no Docker
$ docker start gostack_postgres

```

> Instalar o ORM

```
$ yarn add typeorm

// Instala uma dependencia de uso do typeorm
$ yarn add reflect-metadata
```

> Instala o driver do postgree

```
$ yarn add pg
```

> Criando uma Migration

```
$ yarn typeorm migration:create -n CreateAppointements
```
