import { createConnection } from 'typeorm';

/*
 * Ao declarar o createConnection(); ele busca no sistema o arquivo
 * ormconfig.json com as configurações de acesso ao banco de dados
 *
 */

createConnection();
