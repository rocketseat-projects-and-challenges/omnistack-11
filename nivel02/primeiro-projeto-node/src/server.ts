import 'reflect-metadata';

import express from 'express';
import routes from './routes';

import './database';

const app = express();

// routes se torna um middlleware do projeto
app.use(routes);
// porta que o servidor está escutando
app.listen(3333, () => {
  console.log(' Server started on port 3333 !');
});
